package com.sda.curs.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Company {
    @Id
    private int companyId;

    private String companyName;

    @OneToMany
    private List<Employee> employeeList;

    @OneToMany
    private List<Projects> projectsList;

}
