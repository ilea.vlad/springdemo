package com.sda.curs.entity;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


public class EmployeeProject {

    @Id
    private int employeeProjectId;

    @ManyToOne
    private Employee employee;

    @ManyToOne
    private Projects projects;
}
