package com.sda.curs.controller;

import com.github.javafaker.Faker;
import com.sda.curs.entity.Employee;
import com.sda.curs.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/employees")
public class EmployeeController {


    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/create")
    public ResponseEntity create(@RequestBody Employee employee) {

        return ResponseEntity.ok(employeeService.create(employee));
    }

    @GetMapping("/populate")
    public void faker() {
        Faker faker = new Faker();
        List<Employee> dummyEmployee = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Employee employee = new Employee();
            employee.setFirstName(faker.name().firstName());
            employee.setLastName(faker.name().lastName());
            dummyEmployee.add(employee);
        }
        employeeService.createAll(dummyEmployee);


    }

    @GetMapping("/employees")
    public List<Employee> getAll() {
        return employeeService.getAllEmployee();
    }

    @DeleteMapping("/delete/{employeeId}")
    public void delete(@PathVariable int employeeId){
        employeeService.delete(employeeId);
    }



    @PutMapping("/update/{employeeId}")
    public void update(@PathVariable int employeeId, @RequestBody Employee employee) {
        employeeService.updateEmployee(employeeId,employee);
    }


}
