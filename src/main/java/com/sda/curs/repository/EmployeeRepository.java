package com.sda.curs.repository;

import com.sda.curs.entity.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Integer> {
}
