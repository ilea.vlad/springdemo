package com.sda.curs.service;

import com.sda.curs.entity.Employee;

import java.util.List;

public interface EmployeeService {
    Employee create(Employee employee);
    Iterable<Employee> createAll(List<Employee> employeeList);
    List<Employee> getAllEmployee();
    void delete(int employeeId);
    void updateEmployee(int employeeId, Employee employee);

}
