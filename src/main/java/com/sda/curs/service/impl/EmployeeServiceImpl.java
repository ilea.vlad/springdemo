package com.sda.curs.service.impl;

import com.sda.curs.entity.Employee;
import com.sda.curs.repository.EmployeeRepository;
import com.sda.curs.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Iterable<Employee> createAll(List<Employee> employeeList) {
        return employeeRepository.saveAll(employeeList);
    }

    @Override
    public List<Employee> getAllEmployee() {
        return (List<Employee>) employeeRepository.findAll();
    }

    @Override
    public void delete(int employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @Override
    public void updateEmployee(int employeeId, Employee employee) {
        Optional<Employee> optional= employeeRepository.findById(employeeId);
        optional.ifPresent(employee1 ->{
            employee1.setFirstName(employee.getFirstName());
            employee1.setLastName(employee.getLastName());
            employeeRepository.save(employee1);
        } );



    }




}
